package com.globe.androidclient.view.albumviewer

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.globe.androidclient.R
import com.globe.androidclient.test_minsu.RetrofitAPIInterface
import com.globe.androidclient.test_minsu.RetrofitHelper
import com.globe.androidclient.service.NetworkCheckService
import com.globe.androidclient.test_minsu.ReverseGeocodingResponse
import com.globe.androidclient.util.replaceFragment
import com.globe.androidclient.view.albumviewer.presenter.AlbumVieweFragmentPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AlbumViewerActivity : AppCompatActivity(), NetworkCheckService.ConnectionServiceCallback {


    lateinit var uriList: ArrayList<String>

    companion object {
        const val TAG_INTERVAL = "interval"
        const val TAG_URL_PING = "url_ping"
        const val TAG_ACTIVITY_NAME = "activity_name"
        const val TAG_URI_LIST = "uri_list"
    }

    override fun hasInternetConnection() {
        Log.e("ㅂ", "연결 성공")
    }

    override fun hasNoInternetConnection() {
        Log.e("ㅂ", "연결 실패")
    }

    private lateinit var albumViewrFragmentPresenter: AlbumVieweFragmentPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_album_viewer)
        val albumViewrFragment = AlbumViewerFragment.newInstance()
        replaceFragment(albumViewrFragment, R.id.album_viewer_fragment_container)
        albumViewrFragmentPresenter = AlbumVieweFragmentPresenter(albumViewrFragment, GetConvertAddressInterfaceImpl())




        //장소 변환 확인
        val retrofitAPIInterface = RetrofitHelper.getKakaoLocalClient().create(RetrofitAPIInterface::class.java)
        retrofitAPIInterface.getRequest(127.35342, 36.36127).enqueue(object : Callback<ReverseGeocodingResponse> {
            override fun onFailure(call: Call<ReverseGeocodingResponse>?, t: Throwable?) {
                t?.printStackTrace()
            }

            override fun onResponse(call: Call<ReverseGeocodingResponse>?, response: Response<ReverseGeocodingResponse>?) {
                Log.e("ㅂ", response?.body().toString())

            }

        })


    }
}
