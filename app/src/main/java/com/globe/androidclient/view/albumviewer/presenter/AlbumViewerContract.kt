package com.globe.androidclient.view.albumviewer.presenter

import com.globe.androidclient.BasePresenter
import com.globe.androidclient.BaseView
import com.mapbox.mapboxsdk.geometry.LatLng

/**
 * Created by baeminsu on 03/08/2018.
 */

interface AlbumViewerContract {


    //AlbumViwerFragment 용임

    interface AlbumView : BaseView<AlbumPresenter>{

        fun addRecyclerViewCategorySection(category: String)
    }
    interface AlbumPresenter : BasePresenter {

        fun convertPointToAddress(point: LatLng)
    }


    interface GetConvertAddressInterface {

        fun getConvertAddress(onFinishedConvert: OnFinishedConvert, point: LatLng)

        interface OnFinishedConvert {
            fun onSuccess(address: String)
            fun onFailure(t: Throwable)
        }

    }


    interface DetailPageView : BaseView<DetailPagePresenter>
    interface DetailPagePresenter : BasePresenter


}