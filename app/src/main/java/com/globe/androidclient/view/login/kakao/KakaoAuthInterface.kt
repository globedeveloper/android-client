package com.globe.androidclient.view.login.kakao

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import com.google.gson.annotations.SerializedName


/**
 * Created by baeminsu on 22/07/2018.
 */


interface KakaoAuthInterface {

    companion object {
        val baseURL = "https://us-central1-globe-4ddac.cloudfunctions.net/"
    }

    @Headers("Content-Type: application/json")
    @POST("verifyKakao")
    fun getFirebaseToken(@Body body: String): Call<FirebaseAuthToken>
}
