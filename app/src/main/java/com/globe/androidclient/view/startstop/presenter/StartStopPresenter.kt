package com.globe.androidclient.view.startstop.presenter

import android.content.Context
import com.globe.androidclient.model.local.GPSTrackDao
import com.globe.androidclient.model.local.schema.Episode
import com.globe.androidclient.model.preference.AppPreference
import com.globe.androidclient.util.START_DATETIME
import com.globe.androidclient.util.START_STOP_FLAG
import com.globe.androidclient.util.START_STOP_ID
import com.globe.androidclient.util.STOP_DATETIME
import java.util.*

class StartStopPresenter(startStopView: StartStopContract.View)
    : StartStopContract.Presenter {

    init {
        startStopView.presenter = this
    }

    override fun start() {

    }

    override fun saveStartPreference(context: Context) {
        AppPreference(context).put(START_STOP_FLAG, true)
        AppPreference(context).put(START_DATETIME, Date().time)
    }

    override fun saveStopPreference(context: Context) {
        AppPreference(context).put(START_STOP_FLAG, false)
        AppPreference(context).put(STOP_DATETIME, Date().time)
    }

    override fun saveLocalEpisode(context: Context) {
        val episode = Episode().apply {
            startDate = AppPreference(context).getLong(START_DATETIME)
            stopDate = AppPreference(context).getLong(STOP_DATETIME)
            title = startDate.toString()
        }
        episode.save()
        AppPreference(context).put(START_STOP_ID, episode.id!!)
    }

    override fun getStartTime(context: Context): Long
            = AppPreference(context).getLong(START_DATETIME, 0L)

    override fun isStart(context: Context)
            = AppPreference(context).getBoolean(START_STOP_FLAG, false)

    override fun getDBCount(start: Long)
            = GPSTrackDao.getStartStopGPSTrackCount(start)
}