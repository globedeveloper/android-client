package com.globe.androidclient.view.memorymap.presenter

import android.content.Context
import com.globe.androidclient.model.local.GPSTrackDao
import com.globe.androidclient.model.local.schema.GPSTrack
import com.globe.androidclient.model.preference.AppPreference
import com.globe.androidclient.util.START_DATETIME
import com.globe.androidclient.util.STOP_DATETIME

class MemoryMapPresenter(
        memoryMapView: MemoryMapContract.View
) : MemoryMapContract.Presenter {
    init {
        memoryMapView.presenter = this
    }

    override fun start() {

    }

    override fun getStartStopTrackList(context: Context): List<GPSTrack> = GPSTrackDao.getStartStopGPSTrack(
            AppPreference(context).getLong(START_DATETIME),
            AppPreference(context).getLong(STOP_DATETIME)
    )
}