package com.globe.androidclient.view.memory.presenter

class MemoryPictureListPresenter(
        pictureListView: MemoryContract.PictureListView
) : MemoryContract.PictureListPresenter {

    init {
        pictureListView.presenter = this
    }

    override fun start() {

    }
}