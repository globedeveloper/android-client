package com.globe.androidclient.view.startstop

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.globe.androidclient.R
import com.globe.androidclient.util.replaceFragment
import com.globe.androidclient.view.startstop.presenter.StartStopPresenter

class StartStopActivity : AppCompatActivity() {

    private lateinit var startStopPresenter: StartStopPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start_stop)

        val startStopFragment = StartStopFragment.newInstance("start")

        replaceFragment(startStopFragment, R.id.startStopContainer)

        startStopPresenter = StartStopPresenter(startStopFragment)
    }
}
