package com.globe.androidclient.view.splash

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.globe.androidclient.R
import com.globe.androidclient.view.splash.presenter.SplashContract
import com.globe.androidclient.view.splash.presenter.SplashPresenter
import android.content.Intent
import android.os.Handler
import android.widget.Toast
import com.globe.androidclient.view.login.LoginActivity
import com.globe.androidclient.view.tutorial.TutorialActivity
import com.google.firebase.auth.FirebaseAuth


class SplashActivity : AppCompatActivity(), SplashContract.View {

    override lateinit var presenter: SplashContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        presenter = SplashPresenter(this)
        Handler().postDelayed(Runnable {
            if (FirebaseAuth.getInstance().currentUser != null) {
                Toast.makeText(this, FirebaseAuth.getInstance().currentUser?.email, Toast.LENGTH_SHORT).show()
                startActivity(Intent(this, TutorialActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }, 1000)
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        System.exit(0)
    }

}
