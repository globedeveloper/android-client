package com.globe.androidclient.view.memory

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.globe.androidclient.R
import com.globe.androidclient.view.memory.adapter.PictureListFragmentViewPagerAdapter
import com.globe.androidclient.view.memory.presenter.MemoryContract
import kotlinx.android.synthetic.main.fragment_memory_picture_list.view.*

class MemoryPictureListFragment: Fragment(), MemoryContract.PictureListView {

    override lateinit var presenter: MemoryContract.PictureListPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_memory_picture_list, container, false)

        with(root) {

//            val pictureList = ArrayList<Picture>()
//            pictureList.add(Picture(R.drawable.sample_picture_item))
//            pictureList.add(Picture(R.drawable.sample_picture_item))
//            pictureList.add(Picture(R.drawable.sample_picture_item))
//            pictureList.add(Picture(R.drawable.sample_picture_item))

            memoryPictureListViewPager.apply {
                adapter = PictureListFragmentViewPagerAdapter(activity!!.supportFragmentManager)
            }
        }

        return root
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    companion object {
        fun newInstance() = MemoryPictureListFragment()
    }
}
