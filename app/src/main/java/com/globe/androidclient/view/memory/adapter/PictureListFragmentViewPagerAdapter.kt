package com.globe.androidclient.view.memory.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.globe.androidclient.view.memory.MemoryPicturePageFragment
import com.globe.androidclient.view.tutorial.TutorialPageFragment


class PictureListFragmentViewPagerAdapter(
        fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager) {
    override fun getItem(position: Int)
            = MemoryPicturePageFragment.newInstance()

    override fun getCount() = 4
}