package com.globe.androidclient.view.memorymap.presenter

import android.content.Context
import com.globe.androidclient.BasePresenter
import com.globe.androidclient.BaseView
import com.globe.androidclient.model.local.schema.GPSTrack
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.SupportMapFragment

interface MemoryMapContract {
    interface View: BaseView<Presenter> {

        var startStopTrackList: List<GPSTrack>

        // mapbox
        fun movingCamera(memoryMapFragment: SupportMapFragment, latLngBounds: LatLngBounds)
        fun makeMarkers(memoryMapFragment: SupportMapFragment, markers: List<MarkerOptions>)
        fun makeLines(memoryMapFragment: SupportMapFragment)
    }

    interface Presenter: BasePresenter {
        fun getStartStopTrackList(context: Context): List<GPSTrack>
    }
}