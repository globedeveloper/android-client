package com.globe.androidclient.view.memory.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.globe.androidclient.R
import com.globe.androidclient.model.local.schema.User

class UserListAdapter(
        private val context: Context,
        private val userList: List<User>
) : RecyclerView.Adapter<UserListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            UserListViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.layout_memory_map_user_item, parent, false))

    override fun getItemCount() = userList.size

    override fun onBindViewHolder(holder: UserListViewHolder, position: Int) =
            holder.bind(userList[position])
}