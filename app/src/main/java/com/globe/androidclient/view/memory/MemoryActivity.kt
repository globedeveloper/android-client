package com.globe.androidclient.view.memory

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.globe.androidclient.R
import com.globe.androidclient.view.memory.presenter.MemoryPictureListPresenter
import com.globe.androidclient.view.memory.presenter.MemoryUserListPresenter
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.constants.Style
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMapOptions

class MemoryActivity : AppCompatActivity() {

    private lateinit var memoryUserListPresenter: MemoryUserListPresenter
    private lateinit var memoryPictureListPresenter: MemoryPictureListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_memory)

        val options = MapboxMapOptions().apply {
            styleUrl(Style.LIGHT)
            camera(CameraPosition.Builder()
                    .target(LatLng(36.321655, 127.378953))
                    .zoom(15.0)
                    .build())
        }
        Mapbox.getInstance(this, getString(R.string.mapbox_key))
//
//        val memoryMapFragment = SupportMapFragment.newInstance(options)
//        replaceFragment(memoryMapFragment, R.id.memoryMapContainer)
//        val userListFragment = MemoryUserListFragment.newInstance()
//        replaceFragment(userListFragment, R.id.userListContainer)
//        val pictureListFragment = MemoryPictureListFragment.newInstance()
//        replaceFragment(pictureListFragment, R.id.pictureListContainer)

//        val startStopFragment = MemoryStartStopFragment.newInstance()
//        replaceFragment(startStopFragment,R.id.memoryMapContainer)
//        memoryStartStopPresenter = MemoryStartStopPresenter(startStopFragment)

//        memoryUserListPresenter = MemoryUserListPresenter(userListFragment)
//        memoryPictureListPresenter = MemoryPictureListPresenter(pictureListFragment)
    }
}
