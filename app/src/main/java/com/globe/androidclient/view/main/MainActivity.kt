package com.globe.androidclient.view.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.globe.androidclient.R
import com.globe.androidclient.util.replaceFragment
import com.globe.androidclient.view.main.presenter.MainPresenter

class MainActivity : AppCompatActivity() {

    private lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragment = MainFragment.newInstance()
        replaceFragment(fragment, R.id.mainContainer)
        mainPresenter = MainPresenter(fragment)
    }
}
