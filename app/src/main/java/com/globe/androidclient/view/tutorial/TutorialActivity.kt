package com.globe.androidclient.view.tutorial

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.globe.androidclient.R
import com.globe.androidclient.util.replaceFragment
import com.globe.androidclient.view.tutorial.presenter.TutorialPresenter

class TutorialActivity : AppCompatActivity() {

    private lateinit var tutorialPresenter: TutorialPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        val tutorialFragment = TutorialFragment.newInstance()

        replaceFragment(tutorialFragment, R.id.tutorialContainer)

        tutorialPresenter = TutorialPresenter(tutorialFragment)
    }
}
