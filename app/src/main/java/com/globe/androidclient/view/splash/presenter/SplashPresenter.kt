package com.globe.androidclient.view.splash.presenter

class SplashPresenter(splashView: SplashContract.View)
    : SplashContract.Presenter {

    init {
        splashView.presenter = this
    }

    override fun start() {

    }
}