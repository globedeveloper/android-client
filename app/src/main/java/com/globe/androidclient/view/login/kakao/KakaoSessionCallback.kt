package com.globe.androidclient.view.login.kakao

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.TaskCompletionSource
import com.google.android.gms.tasks.Continuation
import com.globe.androidclient.test_minsu.RetrofitHelper
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.GsonBuilder
import com.kakao.auth.ISessionCallback
import com.kakao.network.ErrorResult
import com.kakao.usermgmt.UserManagement
import com.kakao.usermgmt.callback.MeV2ResponseCallback
import com.kakao.usermgmt.response.MeV2Response
import com.kakao.util.OptionalBoolean
import com.kakao.util.exception.KakaoException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by baeminsu on 21/07/2018.
 */


class KakaoSessionCallback : ISessionCallback {

    private val TAG = "KakaoSessionCallback"


//    List<String> keys = new ArrayList<>();
//    keys.add("properties.nickname");
//    keys.add("properties.profile_image");
//    keys.add("kakao_account.email");

    override fun onSessionOpened() {

        UserManagement.getInstance().me(object : MeV2ResponseCallback() {

            override fun onSuccess(result: MeV2Response?) {

                getFirebaseJWT(result!!).continueWithTask(object : Continuation<String, Task<AuthResult>> {
                    override fun then(p0: Task<String>): Task<AuthResult> {

                        return FirebaseAuth.getInstance().signInWithCustomToken(p0.result)
                    }
                }).addOnCompleteListener(OnCompleteListener {
                    if (it.isSuccessful) {
                        Log.e("깨독", "ㄹ그인")
                    } else {
                        Log.e("깨독", "ㄹ그인")
                    }
                })


            }

            override fun onSessionClosed(errorResult: ErrorResult?) {

            }
        })


    }

    override fun onSessionOpenFailed(exception: KakaoException?) {


    }


    private fun getFirebaseJWT(kakaoUserProfile: MeV2Response): Task<String> {


        val source = TaskCompletionSource<String>();


        val validationObject: HashMap<String, String> = HashMap()

        validationObject.apply {
            put("userId", kakaoUserProfile.id.toString())

            if (kakaoUserProfile.kakaoAccount.hasEmail() == OptionalBoolean.TRUE) {
                Log.e("이메일체크1", kakaoUserProfile.kakaoAccount.hasEmail().toString())
                put("email", kakaoUserProfile.kakaoAccount.email)
            } else {
                Log.e("이메일체크2", kakaoUserProfile.kakaoAccount.hasEmail().toString())
                put("email", "")
            }

            put("nickname", kakaoUserProfile.nickname)
            put("profileImage", kakaoUserProfile.thumbnailImagePath)
        }

        val jsonData: String = GsonBuilder()
                .setLenient()
                .create().toJson(validationObject)


        val kakaoAuthInterface: KakaoAuthInterface? = RetrofitHelper.getKakaoLocalClient().create(KakaoAuthInterface::class.java)

        kakaoAuthInterface?.getFirebaseToken(jsonData)?.enqueue(object : Callback<FirebaseAuthToken> {
            override fun onResponse(call: Call<FirebaseAuthToken>?, response: Response<FirebaseAuthToken>?) {
                if (response?.isSuccessful!!) {
                    val firebaseAuthToken: FirebaseAuthToken = response.body()!!
                    source.setResult(firebaseAuthToken.firebaseToken)
                } else {
                    Log.e(TAG, "카카오 세션 콜백 응답 실패")
                    Log.e(TAG + 2, response?.body().toString())
                    Log.e(TAG + 3, response.message().toString())
                }

            }

            override fun onFailure(call: Call<FirebaseAuthToken>?, t: Throwable?) {
                Log.e(TAG, "카카오 세션 콜백 요청 실")
            }
        })
        return source.task;


    }

}