package com.globe.androidclient.view.splash.presenter

import com.globe.androidclient.BasePresenter
import com.globe.androidclient.BaseView

interface SplashContract {
    interface View: BaseView<Presenter> {
    }

    interface Presenter: BasePresenter {

    }
}