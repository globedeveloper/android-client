package com.globe.androidclient.view.memory.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.globe.androidclient.model.local.schema.User
import kotlinx.android.synthetic.main.layout_memory_map_user_item.view.*

class UserListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(user: User) {
        with(itemView) {
            userName.text = user.name
        }
    }
}