package com.globe.androidclient.view.memory.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.globe.androidclient.R
import com.globe.androidclient.model.local.schema.Picture

class PictureListAdapter(
        private val context: Context,
        private val pictureList: List<Picture>
) : RecyclerView.Adapter<PictureListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            PictureListViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.fragment_memory_map_picture_page, parent, false))

    override fun getItemCount() = pictureList.size

    override fun onBindViewHolder(holder: PictureListViewHolder, position: Int) =
            holder.bind(pictureList[position])
}