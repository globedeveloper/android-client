package com.globe.androidclient.view.main


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.globe.androidclient.R
import com.globe.androidclient.model.local.schema.MemoryCard
import com.globe.androidclient.view.main.adapter.MemoryCardAdapter
import com.globe.androidclient.view.main.presenter.MainContract
import kotlinx.android.synthetic.main.fragment_main.view.*

class MainFragment : Fragment(), MainContract.View {



    override lateinit var presenter: MainContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val root = inflater.inflate(R.layout.fragment_main, container, false)

        with(root) {
            // TODO: 레이아웃 작업
            val datas = ArrayList<MemoryCard>()
            datas.add(MemoryCard("1", "1"))
            datas.add(MemoryCard("2", "2"))
            datas.add(MemoryCard("3", "3"))
            datas.add(MemoryCard("4", "4"))

            val adapter = MemoryCardAdapter(context)
            adapter.addAll(datas)

            cardStackView.setAdapter(adapter)

        }

        return root
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}
