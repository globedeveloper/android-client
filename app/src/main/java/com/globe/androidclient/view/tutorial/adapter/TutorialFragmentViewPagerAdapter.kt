package com.globe.androidclient.view.tutorial.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.globe.androidclient.view.tutorial.TutorialPageFragment


class TutorialFragmentViewPagerAdapter(
        fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager) {
    override fun getItem(position: Int)
            = TutorialPageFragment.newInstance("asdf")

    override fun getCount() = 4
}