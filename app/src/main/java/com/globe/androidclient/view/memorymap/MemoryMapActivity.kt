package com.globe.androidclient.view.memorymap

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.globe.androidclient.R
import com.globe.androidclient.model.local.GPSTrackDao
import com.globe.androidclient.model.local.schema.GPSTrack
import com.globe.androidclient.model.preference.AppPreference
import com.globe.androidclient.util.START_DATETIME
import com.globe.androidclient.util.STOP_DATETIME
import com.globe.androidclient.util.replaceFragment
import com.globe.androidclient.view.memorymap.presenter.MemoryMapContract
import com.globe.androidclient.view.memorymap.presenter.MemoryMapPresenter
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.constants.Style
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMapOptions
import com.mapbox.mapboxsdk.maps.SupportMapFragment
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback


class MemoryMapActivity : AppCompatActivity(), MemoryMapContract.View {

    override lateinit var presenter: MemoryMapContract.Presenter
    override lateinit var startStopTrackList: List<GPSTrack>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, getString(R.string.mapbox_key))
        setContentView(R.layout.activity_memory_map)

        presenter = MemoryMapPresenter(this)

        // DB에서 리스트 가져옴
        startStopTrackList = presenter.getStartStopTrackList(this)

        // 맵박스 옵션
        val options = MapboxMapOptions().apply {
            styleUrl(Style.LIGHT)
        }

        // 맵박스 프레그먼트 생성
        val memoryMapFragment = SupportMapFragment.newInstance(options)
        replaceFragment(memoryMapFragment, R.id.memoryMapBoxContainer)

        // 리스트가 비어있지 않다면
        if (startStopTrackList.isNotEmpty()) {

            // 카메라 바운드를 위한 LatLng List
            val routeLatLngList = startStopTrackList.map {
                LatLng(it.lat, it.lng)
            }.toMutableList()

            // 맵의 초기 바운드 설정
            if(routeLatLngList.size > 1) {
                movingCamera(memoryMapFragment, LatLngBounds.Builder()
                        .includes(routeLatLngList)
                        .build())
            }

            // 마커 만들기
            // TODO : 사진 찍은 갯수대로 마커를 만들기
            makeMarkers(memoryMapFragment, arrayListOf(
                    MarkerOptions().position(routeLatLngList.first()),
                    MarkerOptions().position(routeLatLngList.last())
            ))

            // 선 만들기
            makeLines(memoryMapFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun movingCamera(memoryMapFragment: SupportMapFragment, latLngBounds: LatLngBounds)
            = memoryMapFragment.getMapAsync {
                it.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100))
            }

    override fun makeMarkers(memoryMapFragment: SupportMapFragment, markers: List<MarkerOptions>)
            = memoryMapFragment.getMapAsync {
                it.addMarkers(markers)
            }

    override fun makeLines(memoryMapFragment: SupportMapFragment)
            = memoryMapFragment.getMapAsync {
                // 선을 위한 Point List
                val routeCoordinateList = startStopTrackList.map {
                    Point.fromLngLat(it.lng, it.lat)
                }.toMutableList()

                // 선 만들기
                val pointFeatureList = routeCoordinateList.let {
                    LineString.fromLngLats(it)
                }.let {
                    mutableListOf(Feature.fromGeometry(it))
                }.let {
                    FeatureCollection.fromFeatures(it)
                }
                it.addSource(GeoJsonSource("new-line-source", pointFeatureList))

                val lineLayer = LineLayer("linelayer", "new-line-source")
                lineLayer.setProperties(
                        PropertyFactory.lineDasharray(arrayOf(0.01f, 2f)),
                        PropertyFactory.lineCap(Property.LINE_CAP_ROUND),
                        PropertyFactory.lineJoin(Property.LINE_JOIN_ROUND),
                        PropertyFactory.lineWidth(5f),
                        PropertyFactory.lineColor(Color.parseColor("#e55e5e"))
                )
                it.addLayer(lineLayer)
            }
}