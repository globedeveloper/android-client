package com.globe.androidclient.view.albumviewer

import com.globe.androidclient.test_minsu.RetrofitAPIInterface
import com.globe.androidclient.test_minsu.RetrofitHelper
import com.globe.androidclient.test_minsu.ReverseGeocodingResponse
import com.globe.androidclient.view.albumviewer.presenter.AlbumViewerContract
import com.mapbox.mapboxsdk.geometry.LatLng
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by baeminsu on 12/08/2018.
 */
class GetConvertAddressInterfaceImpl : AlbumViewerContract.GetConvertAddressInterface {

    override fun getConvertAddress(onFinishedConvert: AlbumViewerContract.GetConvertAddressInterface.OnFinishedConvert, point: LatLng) {
        val retrofitAPIInterface = RetrofitHelper.getKakaoLocalClient().create(RetrofitAPIInterface::class.java)

        retrofitAPIInterface.getRequest(point.longitude, point.latitude).enqueue(object : Callback<ReverseGeocodingResponse> {
            override fun onResponse(call: Call<ReverseGeocodingResponse>?, response: Response<ReverseGeocodingResponse>?) {
                val result = response?.body().toString()
                onFinishedConvert.onSuccess(result)
            }
            override fun onFailure(call: Call<ReverseGeocodingResponse>?, t: Throwable?) {
                onFinishedConvert.onFailure(t!!)
            }
        })

    }


}