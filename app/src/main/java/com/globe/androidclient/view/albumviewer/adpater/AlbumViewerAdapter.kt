package com.globe.androidclient.view.albumviewer.adpater

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.globe.androidclient.R
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection
import android.content.Context
import com.globe.androidclient.util.GlideApp

/**
 * Created by baeminsu on 05/08/2018.
 */

class AlbumViewerAdapter(var context: Context) : SectionedRecyclerViewAdapter() {

    var random = 1;

    inner class ExpandablePhotoSection(var headerPlace: String, var list: MutableList<ViewerPhoto>) : StatelessSection(SectionParameters.builder()
            .itemResourceId(R.layout.recyclerview_item_albumview_contents)
            .headerResourceId(R.layout.recyclerview_item_albumview_header)
            .build()) {

        private var expanded = true


        override fun getContentItemsTotal(): Int {
            if (expanded)
                return list.size
            else
                return 0
        }


        override fun getHeaderViewHolder(view: View?): RecyclerView.ViewHolder {
            return HeaderViewHolder(view!!)
        }

        override fun getItemViewHolder(view: View?): RecyclerView.ViewHolder {
            return ContentViewHolder(view!!)
        }



        override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder?) {
            val headerHolder = holder as HeaderViewHolder
            headerHolder.place.text = headerPlace+" (${list.size})"

            headerHolder.view.setOnClickListener {
                expanded = !expanded
                headerHolder.imgArrow.setImageResource(
                        if (expanded) R.drawable.up_arrow
                        else R.drawable.down_arrow
                )
                notifyDataSetChanged()
            }
        }

        override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

            val itemHolder = holder as ContentViewHolder

//            var uri = list.get(position).photoUri
            var placeCategory = list.get(position).place



            when (random) {
                1 -> GlideApp.with(context)
                        .load(R.drawable.sample_image1)
                        .into(itemHolder.photo)
                2 -> GlideApp.with(context)
                        .load(R.drawable.sample_image2)
                        .into(itemHolder.photo)
                3 -> GlideApp.with(context)
                        .load(R.drawable.sample_image3)
                        .into(itemHolder.photo)
                4 -> GlideApp.with(context)
                        .load(R.drawable.sample_image4)
                        .into(itemHolder.photo)
                5 -> GlideApp.with(context)
                        .load(R.drawable.sample_image5)
                        .into(itemHolder.photo)
                6 -> GlideApp.with(context)
                        .load(R.drawable.sample_image6)
                        .into(itemHolder.photo)
            }
            random++
            if (random == 7) random = 1
        }


    }


    inner class HeaderViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var place: TextView = view.findViewById(R.id.album_viewer_item_header_title)
        var imgArrow: ImageView = view.findViewById(R.id.album_viewer_item_header_arrow)

    }

    inner class ContentViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var photo: ImageView = view.findViewById(R.id.album_viewer_item_contents_image)

    }


    inner class ViewerPhoto(var place: String) {

    }

}