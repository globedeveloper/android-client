package com.globe.androidclient.view.startstop.presenter

import android.content.Context
import com.globe.androidclient.BasePresenter
import com.globe.androidclient.BaseView

interface StartStopContract {
    interface View : BaseView<Presenter> {
        fun startStopViewContentChange()
        fun changeStopView()

        fun permissionUserCheck()
        fun isPermissionGranted(): Boolean
    }

    interface Presenter : BasePresenter {

        // preference에 저장
        fun saveStartPreference(context: Context)
        fun saveStopPreference(context: Context)

        // 로컬 디비에 저장(제목이랑 스타트, 스탑)
        fun saveLocalEpisode(context: Context)

        // 시작했는지
        fun isStart(context: Context): Boolean
        fun getStartTime(context: Context): Long

        fun getDBCount(start: Long): Long
    }
}