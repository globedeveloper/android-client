package com.globe.androidclient.view.tutorial.presenter


class TutorialPresenter(tutorialView: TutorialContract.View)
    : TutorialContract.Presenter {

    init {
        tutorialView.presenter = this
    }

    override fun start() {

    }
}