package com.globe.androidclient.view.memory.presenter

class MemoryUserListPresenter(
        userListView: MemoryContract.UserListView
) : MemoryContract.UserListPresenter {

    init {
        userListView .presenter = this
    }

    override fun start() {

    }
}