package com.globe.androidclient.view.memory

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.globe.androidclient.R


class MemoryPicturePageFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_memory_map_picture_page, container, false)

        with(root) {

        }

        return root
    }

    companion object {
        fun newInstance() = MemoryPicturePageFragment()
    }
}
