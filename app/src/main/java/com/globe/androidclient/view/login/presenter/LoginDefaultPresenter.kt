package com.globe.androidclient.view.login.presenter

class LoginDefaultPresenter(
        defaultView: LoginContract.DefaultView
) : LoginContract.DefaultPresenter {

    init {
        defaultView.presenter = this
    }

    override fun start() {
    }
}