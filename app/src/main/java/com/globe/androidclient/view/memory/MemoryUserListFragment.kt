package com.globe.androidclient.view.memory

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.HORIZONTAL
import com.globe.androidclient.R
import com.globe.androidclient.model.local.schema.User
import com.globe.androidclient.view.memory.adapter.UserListAdapter
import com.globe.androidclient.view.memory.presenter.MemoryContract
import kotlinx.android.synthetic.main.fragment_memory_user_list.view.*

class MemoryUserListFragment: Fragment(), MemoryContract.UserListView {

    override lateinit var presenter: MemoryContract.UserListPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_memory_user_list, container, false)

        with(root) {

            val userList = ArrayList<User>()
            userList.add(User("ME"))
            userList.add(User("EUNBIN"))
            userList.add(User("MINSU"))
            userList.add(User("SANGWOO"))

            memoryUserListRecyclerView.apply {
                layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
                adapter = UserListAdapter(context, userList)
            }
        }

        return root
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    companion object {
        fun newInstance() = MemoryUserListFragment()
    }
}
