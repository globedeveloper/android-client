package com.globe.androidclient.view.tutorial

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.globe.androidclient.R
import com.globe.androidclient.view.main.MainActivity
import com.globe.androidclient.view.tutorial.adapter.TutorialFragmentViewPagerAdapter
import com.globe.androidclient.view.tutorial.presenter.TutorialContract
import kotlinx.android.synthetic.main.fragment_tutorial.view.*
import android.support.v4.view.ViewPager
import android.view.View.GONE
import android.view.View.VISIBLE
import com.globe.androidclient.view.startstop.StartStopActivity


class TutorialFragment : Fragment(), TutorialContract.View {

    override lateinit var presenter: TutorialContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_tutorial, container, false)

        with(root) {
            tutorialViewPager.adapter =
                    TutorialFragmentViewPagerAdapter(activity?.supportFragmentManager!!)
            tutorialViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }

                override fun onPageSelected(position: Int) {
                    tutorialPageIndicatorView.selection = position
                    if(position == 3) {
                        tutorialMainButton.visibility = VISIBLE
                    } else {
                        tutorialMainButton.visibility = GONE
                    }
                }

                override fun onPageScrollStateChanged(state: Int) {
                }
            })
            tutorialMainButton.setOnClickListener {
                startActivity(Intent(activity, StartStopActivity::class.java))
            }
        }

        return root
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    companion object {
        fun newInstance() = TutorialFragment()
    }
}
