package com.globe.androidclient.view.memory.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.globe.androidclient.model.local.schema.Picture

class PictureListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(picture: Picture) {
        picture.drawableRes
    }
}