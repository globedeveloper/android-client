package com.globe.androidclient.view.login.presenter

import com.globe.androidclient.BasePresenter
import com.globe.androidclient.BaseView
import com.google.android.gms.auth.api.signin.GoogleSignInClient

/**
 * Created by baeminsu on 19/07/2018.
 */


interface LoginContract {


    // LoginDefault
    interface DefaultView : BaseView<DefaultPresenter> {
        fun changeLoginSignUp(isChecked: Boolean)

        fun verifyLocalCheck(): Boolean
        fun loginSubmit()
        fun registerSubmit()
    }

    interface DefaultPresenter : BasePresenter {

    }

    // LoginSelect
    interface View : BaseView<Presenter> {

        var googleSignInClient: GoogleSignInClient?

        fun googleLogin()
        fun facebookLogin()
        fun kakaoLogin()
        fun defaultLogin()

    }

    interface Presenter : BasePresenter {


    }

    interface DefaultFragmentListener {
        fun addDefaultFragment()
    }
}