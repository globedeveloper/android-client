package com.globe.androidclient.view.albumviewer.presenter

import android.util.Log
import com.globe.androidclient.test_minsu.RetrofitAPIInterface
import com.globe.androidclient.test_minsu.RetrofitHelper
import com.mapbox.mapboxsdk.geometry.LatLng
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by baeminsu on 03/08/2018.
 */

class AlbumVieweFragmentPresenter(var albumView: AlbumViewerContract.AlbumView, var getConvertAddressInterface: AlbumViewerContract.GetConvertAddressInterface)
    : AlbumViewerContract.AlbumPresenter, AlbumViewerContract.GetConvertAddressInterface.OnFinishedConvert {

    init {
        albumView.presenter = this
    }

    override fun convertPointToAddress(point: LatLng) {
        getConvertAddressInterface.getConvertAddress(this, point)
    }

    override fun start() {
        //TODO 아직 할 것 없음
    }

    override fun onSuccess(address: String) {
        //TODO 어드레스 변환 성공
        Log.e("헤이헤이","성공")
        albumView

    }

    override fun onFailure(t: Throwable) {
        //TODO 어드레스 변환 실패
        Log.e("헤이헤이","실패")
    }

}