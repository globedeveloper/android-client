package com.globe.androidclient.view.main.presenter

import com.globe.androidclient.BasePresenter
import com.globe.androidclient.BaseView

interface MainContract {
    interface View: BaseView<Presenter> {

    }

    interface Presenter: BasePresenter {

    }
}