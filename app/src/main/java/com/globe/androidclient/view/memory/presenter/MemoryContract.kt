package com.globe.androidclient.view.memory.presenter

import android.content.Context
import com.globe.androidclient.BasePresenter
import com.globe.androidclient.BaseView

interface MemoryContract {

    interface PictureListView : BaseView<PictureListPresenter> {

    }

    interface PictureListPresenter : BasePresenter {

    }

    interface UserListView : BaseView<UserListPresenter> {

    }

    interface UserListPresenter : BasePresenter {

    }


    interface StartStopView : BaseView<StartStopPresenter> {
    }

    interface StartStopPresenter : BasePresenter {

        fun requestStartDay(context: Context)
        fun requestStopDay(context: Context)
        fun getPhotoUriList(context: Context)
    }

}