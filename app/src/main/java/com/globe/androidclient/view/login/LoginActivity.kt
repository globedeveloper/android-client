package com.globe.androidclient.view.login

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.globe.androidclient.R
import com.globe.androidclient.util.addFragment
import com.globe.androidclient.util.removeFragmentById
import com.globe.androidclient.util.replaceFragment
import com.globe.androidclient.view.login.presenter.LoginContract
import com.globe.androidclient.view.login.presenter.LoginDefaultPresenter
import com.globe.androidclient.view.login.presenter.LoginPresenter

class LoginActivity : AppCompatActivity(), LoginContract.DefaultFragmentListener {

    private lateinit var loginPresenter: LoginPresenter
    private var defaultPresenter: LoginDefaultPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_activitiy)

        val fragment = LoginSelectFragment.newInstance()
        replaceFragment(fragment, R.id.loginFragmentContainer)
        loginPresenter = LoginPresenter(fragment)
    }

    override fun addDefaultFragment() {
        val fragment = LoginDefaultFragment.newInstance()
        addFragment(fragment, R.id.loginFragmentContainer)
        defaultPresenter = LoginDefaultPresenter(fragment)
    }

    override fun onBackPressed() {
        if(defaultPresenter != null) {
            removeFragmentById(R.id.loginFragmentContainer)
            defaultPresenter = null
        } else {
            super.onBackPressed()
        }
    }
}
