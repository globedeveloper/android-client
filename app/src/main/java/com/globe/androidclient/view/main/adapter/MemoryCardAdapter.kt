package com.globe.androidclient.view.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.globe.androidclient.R
import com.globe.androidclient.model.local.schema.MemoryCard
import kotlinx.android.synthetic.main.layout_memory_card.view.*

class MemoryCardAdapter(context: Context)
    : ArrayAdapter<MemoryCard>(context, 0){

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val viewHolder: ViewHolder
        var convertView = convertView

        if (convertView == null) {
            val inflater = LayoutInflater.from(context)
            convertView = inflater.inflate(R.layout.layout_memory_card, parent, false)
            viewHolder = ViewHolder(convertView)
            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
        }

//        val memoryCard = getItem(position)
//        viewHolder.title.text = memoryCard.Title

        return convertView!!
    }

    inner class ViewHolder(view: View) {
        lateinit var title: TextView
        lateinit var date: TextView

        init {
            with(view) {
                title = memoryTitle
                date = memoryDate
            }
        }
    }
}