package com.globe.androidclient.view.tutorial.presenter

import com.globe.androidclient.BasePresenter
import com.globe.androidclient.BaseView

interface TutorialContract {
    interface View: BaseView<Presenter> {
    }

    interface Presenter: BasePresenter {

    }
}