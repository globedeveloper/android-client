package com.globe.androidclient.view.login

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import com.globe.androidclient.R
import com.globe.androidclient.view.login.presenter.LoginContract
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_default_login_signup.*
import kotlinx.android.synthetic.main.fragment_default_login_signup.view.*

class LoginDefaultFragment: Fragment(), LoginContract.DefaultView {
    override lateinit var presenter: LoginContract.DefaultPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(
                R.layout.fragment_default_login_signup,
                container, false)
        with(root)  {

            loginDefaultSubmitButton.setOnClickListener {
                if(loginDefaultSignUpToggle.isChecked) {
                    // 로그인
                    loginSubmit()
                } else {
                    // 회원가입
                    registerSubmit()
                }
            }

            loginDefaultForgetText.setOnClickListener {
                FirebaseAuth.getInstance().signOut()
            }

            loginDefaultSignUpToggle.setOnClickListener {
                changeLoginSignUp(loginDefaultSignUpToggle.isChecked)
            }
        }

        return root
    }

    override fun verifyLocalCheck(): Boolean {
        // TODO : 이메일 비밀번호 잘 써졌는지 확인해야됨
        return true
    }

    override fun loginSubmit() {
        if(!verifyLocalCheck()) return
        val auth = FirebaseAuth.getInstance()

        val email = loginDefaultIdEdit.text.toString()
        val password = loginDefaultPasswordEdit.text.toString()

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity!!, object: OnCompleteListener<AuthResult> {
                    override fun onComplete(task: Task<AuthResult>) {
                        if (task.isSuccessful) {
                            Toast.makeText(activity,
                                    "${auth.currentUser!!} ", LENGTH_LONG).show()
                        } else {
                            task.exception?.printStackTrace()
                            Toast.makeText(activity,
                                    "${task.exception?.message} ", LENGTH_LONG).show()
                        }
                    }
                })
    }

    override fun registerSubmit() {
        if(!verifyLocalCheck()) return

    }

    override fun changeLoginSignUp(isChecked: Boolean) {
        if(isChecked) {
            // 로그인 -> 회원가입
            loginDefaultSubmitButton.text = getString(R.string.login_default_submit_signup_text)
            loginDefaultForgetText.visibility = GONE
        } else {
            // 회원가입 -> 로그인
            loginDefaultSubmitButton.text = getString(R.string.login_default_submit_login_text)
            loginDefaultForgetText.visibility = VISIBLE
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }


    companion object {
        fun newInstance() = LoginDefaultFragment()
    }
}