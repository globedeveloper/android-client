package com.globe.androidclient.view.startstop


import android.Manifest.permission.*
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.content.res.AppCompatResources.getDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.globe.androidclient.R
import com.globe.androidclient.service.GPSTrackingService
import com.globe.androidclient.view.memorymap.MemoryMapActivity
import com.globe.androidclient.view.startstop.presenter.StartStopContract
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import kotlinx.android.synthetic.main.fragment_start_stop.*
import kotlinx.android.synthetic.main.fragment_start_stop.view.*
import java.util.ArrayList


private const val ARG_PARAM1 = "param1"

class StartStopFragment : Fragment(), StartStopContract.View, View.OnClickListener {
    private var param1: String? = null

    private var startTime: Long = 0L

    override lateinit var presenter: StartStopContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_start_stop, container, false)
        with(root) {
            startStopButton.setOnClickListener(this@StartStopFragment)
        }
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (presenter.isStart(context!!)) {
            changeStopView()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun startStopViewContentChange() {

        // 시작했는지 안했는지 체크
        if (presenter.isStart(context!!)) {
            // 정지를 눌렀을 경우

            // 서비스 정지
            try {
                activity?.stopService(Intent(context, GPSTrackingService::class.java))
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // SharedPreference에 정지했다고 등록
            presenter.saveStopPreference(context!!)

            // 메모리맵 액티비티(MemoryMapActivity)로 이동
            if (presenter.getDBCount(startTime) > 0L) {
                startActivity(Intent(context, MemoryMapActivity::class.java))

                // 로컬 에피소드에 시작 정지 시간 저장
                presenter.saveLocalEpisode(context!!)
            }
            activity?.finish()

        } else {
            // 시작을 눌렀을 경우

            // 서비스 시작
            activity?.startService(Intent(context, GPSTrackingService::class.java))

            // SharedPreference에 시작했다고 등록
            presenter.saveStartPreference(context!!)
            startTime = presenter.getStartTime(context!!)

            // stop 화면으로 바꿈
            changeStopView()

            // DB 카운트
            countThreadStart()
        }
    }

    private fun countThreadStart() {
        savedDBCount.text = "%d개".format(presenter.getDBCount(startTime))
        Thread {
            while(true) {
                try {
                    activity!!.runOnUiThread {
                        savedDBCount.text = "%d개".format(presenter.getDBCount(startTime))
                    }
                    Thread.sleep(3000)
                } catch (e: Exception) {
                    break
                }
            }
        }.start()
    }

    override fun changeStopView() {
        startStopButton.setImageDrawable(getDrawable(context!!, R.drawable.stop_icon))
    }

    override fun isPermissionGranted(): Boolean = TedPermission.isGranted(context, ACCESS_FINE_LOCATION)

    override fun permissionUserCheck() = TedPermission.with(context)
            .setDeniedMessage("권한을 꼭 설정해 주셔야 합니다!")
            .setPermissionListener(object : PermissionListener {
                override fun onPermissionGranted() {
                    startStopViewContentChange()
                }

                override fun onPermissionDenied(deniedPermissions: ArrayList<String>?) {

                }
            })
            .setPermissions(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE,
                    ACCESS_NETWORK_STATE, ACCESS_FINE_LOCATION)
            .check()

    override fun onClick(v: View?) = when (v?.id) {
        R.id.startStopButton -> {
            if (isPermissionGranted()) {
                startStopViewContentChange()
            } else {
                permissionUserCheck()
            }
        }

        else -> {
        }
    }

    companion object {
        fun newInstance(param1: String) =
                StartStopFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                    }
                }
    }
}
