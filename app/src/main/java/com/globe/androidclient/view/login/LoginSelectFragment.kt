package com.globe.androidclient.view.login

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.globe.androidclient.R
import com.globe.androidclient.view.login.presenter.LoginContract
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions

import com.google.android.gms.common.api.ApiException

import android.util.Log
import android.widget.Button

import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.globe.androidclient.view.login.kakao.KakaoSessionCallback
import com.google.android.gms.common.SignInButton
import com.google.firebase.auth.*
import kotlinx.android.synthetic.main.fragment_login_select.*
import com.google.firebase.auth.FirebaseUser
import com.kakao.auth.Session

class LoginSelectFragment : Fragment(), LoginContract.View {

    private val TAG = "LoginSelectFragment"
    private lateinit var auth: FirebaseAuth
    override var googleSignInClient: GoogleSignInClient? = null
    lateinit var callbackManager: CallbackManager  // Facebook CallBack Mananger
    override lateinit var presenter: LoginContract.Presenter

    private lateinit var defaultFragmentListener: LoginContract.DefaultFragmentListener

    private enum class Login(val num: Int) {
        GOOGLE(10), KAKAO(20), FACEBOOK(30)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if(context is LoginContract.DefaultFragmentListener) {
            defaultFragmentListener = context
        }
    }

    companion object {
        fun newInstance() = LoginSelectFragment()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_login_select, container, false)

        auth = FirebaseAuth.getInstance()
        with(root) {

            val googleLoginFakeBtn: Button = findViewById(R.id.googleLoginFakeBtn)
            val googleLoginBtn: SignInButton = findViewById(R.id.googleLoginBtn)
            val facebookLoginFakeBtn: Button = findViewById(R.id.facebookLoginFakeBtn)
            val facebookLoginBtn: LoginButton = findViewById(R.id.facebookLoginBtn)
            val kakaoLoginFakeBtn: Button = findViewById(R.id.kakaoLoginFakeBtn)
            val kakaoLoginBtn: com.kakao.usermgmt.LoginButton = findViewById(R.id.kakaoLoginBtn)
            val defaultLoginBtn: Button = findViewById(R.id.defaultLoginBtn)

            //Login Click Listener Add
            googleLoginFakeBtn.setOnClickListener {
                //                googleLoginBtn.performClick()
                googleLogin()
            }
            googleLoginBtn.setOnClickListener {

            }
            facebookLoginFakeBtn.setOnClickListener {
                facebookLoginBtn.performClick()
            }
            facebookLoginBtn.setOnClickListener {
                facebookLogin()
            }
            kakaoLoginFakeBtn.setOnClickListener {
                kakaoLogin()
                kakaoLoginBtn.performClick()
            }
            defaultLoginBtn.setOnClickListener {
                defaultLogin()
            }
            return root
        }
    }

    override fun googleLogin() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_google_web_client_id))
                .requestEmail()
                .build()
        googleSignInClient = GoogleSignIn.getClient(activity!!, gso)
        val signInIntent = googleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, Login.GOOGLE.num)
    }

    override fun facebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        facebookLoginBtn.setReadPermissions("email", "public_profile")
        facebookLoginBtn.fragment = this;
        facebookLoginBtn.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {

                Log.d(TAG, "페이스북 로그인 중 성공")
                handleFacebookAccessToken(result!!.accessToken)
            }

            override fun onCancel() {
                Log.d(TAG, "페이스북 로그인 중 취소")
            }

            override fun onError(error: FacebookException?) {
                Log.d(TAG, "페이스북 로그인 중 실패")
            }
        })
    }

    override fun kakaoLogin() {
        val kakaoSessionCallback = KakaoSessionCallback()
        Session.getCurrentSession().addCallback(kakaoSessionCallback)
    }

    override fun defaultLogin() {
        defaultFragmentListener.addDefaultFragment()
    }

    //Google 추가 메소드
    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        val credential: AuthCredential = GoogleAuthProvider.getCredential(acct.idToken, null)
        auth.signInWithCredential(credential)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        Log.d(TAG, "구글로그인 credential : success");
                        val user: FirebaseUser = auth.currentUser!!
                        //TODO 로그인성공
                        loginTest.text = "구글 로그인 성공 핫"
                    } else {
                        Log.w(TAG, "구글로그인 credential : failure", it.exception);
                        //TODO 로그인 실패
                        loginTest.text = "구글 로그인 앗!.. 아아..."
                    }
                }
    }

    //Facebook 추가 메소드
    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        val credentail = FacebookAuthProvider.getCredential(token.token)
        auth.signInWithCredential(credentail).addOnCompleteListener {
            if (it.isSuccessful) {
                loginTest.text = "페이스북 로그인 성공 핫"
                val user = auth.currentUser
            } else {
                loginTest.text = "페이스북 로그인 앗!.. 아아..."
            }
        }
    }

    private fun kakaoUnlink(){

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //페이스북


        //구글
        if (requestCode == Login.GOOGLE.num) {
            val task: com.google.android.gms.tasks.Task<GoogleSignInAccount>? = GoogleSignIn.getSignedInAccountFromIntent(data)

            try {
                val account = task?.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: Exception) {
                Log.w(TAG, "구글 로그인 Activity Reusult Fail", e);
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

}