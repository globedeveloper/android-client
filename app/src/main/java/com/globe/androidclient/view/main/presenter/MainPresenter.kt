package com.globe.androidclient.view.main.presenter

class MainPresenter(mainView: MainContract.View)
    : MainContract.Presenter {

    init {
        mainView.presenter = this
    }

    override fun start() {

    }
}