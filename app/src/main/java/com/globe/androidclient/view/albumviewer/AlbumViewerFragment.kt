package com.globe.androidclient.view.albumviewer


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.globe.androidclient.R
import com.globe.androidclient.util.SpaceItemDecoration
import com.globe.androidclient.view.albumviewer.adpater.AlbumViewerAdapter
import com.globe.androidclient.view.albumviewer.presenter.AlbumViewerContract
import com.mapbox.mapboxsdk.geometry.LatLng
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter


class AlbumViewerFragment : Fragment(), AlbumViewerContract.AlbumView {

    override lateinit var presenter: AlbumViewerContract.AlbumPresenter
    private lateinit var adapter: AlbumViewerAdapter
    private lateinit var photoHashMap: HashMap<String,ArrayList<String>>



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_album_viewer, container, false)
        adapter = AlbumViewerAdapter(context!!)

        adapter.addSection(adapter.ExpandablePhotoSection("대전 어은동", test1())) // 여기 리스트
        adapter.addSection(adapter.ExpandablePhotoSection("대전 효동", test2()))
        adapter.addSection(adapter.ExpandablePhotoSection("대전 행운동", test3()))

        with(root) {
            val recyclerView = findViewById<RecyclerView>(R.id.album_viewer_fragment_recyclerview)
            val glm = GridLayoutManager(context, 4)
            glm.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    when (adapter.getSectionItemViewType(position)) {
                        SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER -> return 4
                        else -> return 1
                    }
                }
            }
            recyclerView.layoutManager = glm
            recyclerView.adapter = adapter
            recyclerView.addItemDecoration(SpaceItemDecoration(5))
        }
        presenter.convertPointToAddress(LatLng(36.36127, 127.35342))
        return root
    }


    fun test1(): MutableList<AlbumViewerAdapter.ViewerPhoto> {
        val list = mutableListOf<AlbumViewerAdapter.ViewerPhoto>()
        list.add(adapter.ViewerPhoto("대전 어은동"))
        list.add(adapter.ViewerPhoto("대전 어은동"))
        list.add(adapter.ViewerPhoto("대전 어은동"))
        return list
    }

    fun test2(): MutableList<AlbumViewerAdapter.ViewerPhoto> {
        val list = mutableListOf<AlbumViewerAdapter.ViewerPhoto>()
        list.add(adapter.ViewerPhoto("대전 효동"))
        list.add(adapter.ViewerPhoto("대전 효동"))
        list.add(adapter.ViewerPhoto("대전 효동"))
        list.add(adapter.ViewerPhoto("대전 효동"))
        list.add(adapter.ViewerPhoto("대전 효동"))
        return list
    }

    fun test3(): MutableList<AlbumViewerAdapter.ViewerPhoto> {
        val list = mutableListOf<AlbumViewerAdapter.ViewerPhoto>()
        list.add(adapter.ViewerPhoto("대전 행운동"))
        list.add(adapter.ViewerPhoto("대전 행운동"))
        list.add(adapter.ViewerPhoto("대전 행운동"))

        return list
    }


    companion object {
        fun newInstance() = AlbumViewerFragment()
    }

    fun addAdapterAddSection(placeName: String, list: MutableList<AlbumViewerAdapter.ViewerPhoto>) {
        adapter.addSection(adapter.ExpandablePhotoSection(placeName, list))
    }

    override fun addRecyclerViewCategorySection(category: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}