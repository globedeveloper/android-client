package com.globe.androidclient.model.local

import com.raizlabs.android.dbflow.annotation.Database


/**
 * Created by baeminsu on 01/08/2018.
 */
@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
object AppDatabase {

    const val NAME = "AppDatabase"

    const val VERSION = 2
}