package com.globe.androidclient.model.local.schema

import android.support.annotation.DrawableRes

data class Picture(
        @DrawableRes val drawableRes: Int

)