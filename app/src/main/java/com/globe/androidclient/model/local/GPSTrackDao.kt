package com.globe.androidclient.model.local

import com.globe.androidclient.model.local.schema.GPSTrack
import com.globe.androidclient.model.local.schema.GPSTrack_Table
import com.raizlabs.android.dbflow.kotlinextensions.greaterThanOrEq
import com.raizlabs.android.dbflow.kotlinextensions.lessThanOrEq
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.sql.language.Select

object GPSTrackDao : GPSTrackDaoImp {
    override fun insertGPSTrack(gpsTrack: GPSTrack): Boolean = gpsTrack.save()
    override fun getAllGPSTrack(): MutableList<GPSTrack>
            = Select().from(GPSTrack::class.java).where().queryList()

    override fun getStartStopGPSTrack(start: Long, stop: Long): MutableList<GPSTrack>
            = Select().from(GPSTrack::class.java)
            .where (GPSTrack_Table.date.greaterThan(start))
            .and(GPSTrack_Table.date.lessThanOrEq(stop))
            .queryList()

    override fun getStartStopGPSTrackCount(start: Long): Long
            = SQLite.selectCountOf().from(GPSTrack::class.java)
            .where (GPSTrack_Table.date.greaterThan(start))
            .longValue()
}