package com.globe.androidclient.model.preference

import android.content.Context
import net.grandcentrix.tray.TrayPreferences

class AppPreference(context: Context)
    : TrayPreferences(context, "app", 1) {
}