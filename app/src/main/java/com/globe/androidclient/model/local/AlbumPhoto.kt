package com.globe.androidclient.model

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import android.support.media.ExifInterface
import android.util.Log
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by baeminsu on 25/07/2018.
 */
fun getPhotoStartStopUriList(context: Context, startDate: Date, endDate: Date): List<Uri> {
    val projection = arrayOf(MediaStore.Images.Media.DATA)
    val contentResolver: ContentResolver = context.contentResolver
    val cursor = contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            projection,
            null,
            null,
            null)
    val result = ArrayList<Uri>(cursor.count)
    val dataColumnIndex = cursor.getColumnIndex(projection[0])
    when {
        cursor == null -> Log.e("사진", "이미지 커서 널")
        cursor.moveToNext() -> do {
            val filePath = cursor.getString(dataColumnIndex)
            val imageUri = Uri.parse(filePath)
            val exif = ExifInterface(filePath)
            val strDate = exif.getAttribute(ExifInterface.TAG_DATETIME)
            try {
                val date = SimpleDateFormat("yyyy:MM:dd HH:mm:ss").parse(strDate);
                if ((date >= startDate) && (date <= endDate))
                    result.add(imageUri)
            } catch (e: Exception) {
//                    Log.e("날짜 정보가 없으면 사실상 카메라로 찍은거 아님 ㅅㄱ", "11")
            }
        } while (cursor.moveToNext())
        else -> Log.e("사진", "이미지 커서, 앨범에 이미지가 없음")
    }
    cursor.close()
    Log.e("그래서 몇개냐 ", "${result.size}개")
    return result
}