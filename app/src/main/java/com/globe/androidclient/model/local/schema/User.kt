package com.globe.androidclient.model.local.schema


/**
 * Created by baeminsu on 01/08/2018.
 */

data class User(
        val name: String
)