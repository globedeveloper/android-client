package com.globe.androidclient.model.local

import com.globe.androidclient.model.local.schema.GPSTrack
import java.util.*

interface GPSTrackDaoImp {
    fun insertGPSTrack(gpsTrack: GPSTrack): Boolean
    fun getAllGPSTrack(): MutableList<GPSTrack>
    fun getStartStopGPSTrack(start: Long, stop: Long = Date().time): MutableList<GPSTrack>
    fun getStartStopGPSTrackCount(start: Long): Long
}