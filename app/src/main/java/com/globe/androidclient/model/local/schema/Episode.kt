package com.globe.androidclient.model.local.schema

import android.support.annotation.Nullable
import com.globe.androidclient.model.local.AppDatabase
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.OneToMany
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.kotlinextensions.*
import com.raizlabs.android.dbflow.structure.BaseModel
import java.util.*

/**
 * Created by baeminsu on 02/08/2018.
 */

@Table(name = "episode", database = AppDatabase::class)
class Episode: BaseModel() {

    @Nullable
    @PrimaryKey(autoincrement = true) // at least one primary key required
    var id: Long? = 0L

    @Column
    var title: String? = ""

    @Column
    var startDate: Long? = 0L

    @Column
    var stopDate: Long? = 0L

    @get:OneToMany(methods = [OneToMany.Method.ALL])
    var photos by oneToMany { select from(Photo::class) where (Photo_Table.id.eq(id)) }
}

@Table(name = "photo", database = AppDatabase::class)
class Photo(
        @Nullable
        @PrimaryKey(autoincrement = true)
        var id: Long? = 0L,

        @Column
        var path: String? = null
)