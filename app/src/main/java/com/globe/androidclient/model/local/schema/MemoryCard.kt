package com.globe.androidclient.model.local.schema

data class MemoryCard(
        val Title: String,
        val Date: String? = null
)