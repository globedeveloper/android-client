package com.globe.androidclient.model.local.schema

import android.support.annotation.Nullable
import com.globe.androidclient.model.local.AppDatabase
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import  com.raizlabs.android.dbflow.structure.BaseModel
import java.util.*

@Table(name = "tracks", database = AppDatabase::class)
data class GPSTrack(
        @Nullable
        @PrimaryKey(autoincrement = true)
        @Column(name = "id")
        var id: Long = 0L,

        @Column(name = "date")
        var date: Long = Date().time,

        @Column(name = "latitude")
        var lat: Double = 0.0,

        @Column(name = "longitude")
        var lng: Double = 0.0
) : BaseModel() {
        override fun toString(): String {
                return date.toString() + " // " + lat + " // " + lng
        }
}