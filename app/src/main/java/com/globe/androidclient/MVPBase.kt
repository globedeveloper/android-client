package com.globe.androidclient

interface BasePresenter {
    fun start()
}

interface BaseView<T> {
    var presenter: T
}