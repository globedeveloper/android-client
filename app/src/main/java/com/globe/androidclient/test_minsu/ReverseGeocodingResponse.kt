package com.globe.androidclient.test_minsu

import com.google.gson.annotations.SerializedName
import org.json.JSONObject

/**
 * Created by baeminsu on 09/08/2018.
 */


data class ReverseGeocodingResponse(

        @SerializedName("meta")
        val meta: Meta? = Meta(),

        @SerializedName("documents")
        val document: List<Document>? = null
)

data class Meta(
        @SerializedName("total_count")
        val totalCount: Int? = 0
)

data class Document(
        @SerializedName("region_type")
        val regionType: String? = "",

        @SerializedName("code")
        val cod: String? = "",

        @SerializedName("address_name")
        val addressNmae: String? = "",

        @SerializedName("region_1depth_name")
        val region1: String? = "",

        @SerializedName("region_2depth_name")
        val region2: String? = "",

        @SerializedName("region_3depth_name")
        val region3: String? = "",

        @SerializedName("region_4depth_name")
        val region4: String? = "",

        @SerializedName("x")
        val lng: String? = "",

        @SerializedName("y")
        val lat: String? = ""


)

