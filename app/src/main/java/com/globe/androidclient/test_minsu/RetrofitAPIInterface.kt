package com.globe.androidclient.test_minsu

import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by baeminsu on 09/08/2018.
 */

interface RetrofitAPIInterface {

    @GET("/v2/local/geo/coord2regioncode.json")
    fun getRequest(@Query("x") longitude: Double, @Query("y") latitude: Double): Call<ReverseGeocodingResponse>


}