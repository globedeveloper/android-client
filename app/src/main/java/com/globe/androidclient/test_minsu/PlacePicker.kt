package com.globe.androidclient.test_minsu

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.globe.androidclient.R
import com.globe.androidclient.util.PlacePickerUtil
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import kotlinx.android.synthetic.main.activity_test.*

/**
 * Created by baeminsu on 21/08/2018.
 */
class PlacePicker : AppCompatActivity() {

    val PLACE_PICKER_REQUEST = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        btn111.setOnClickListener {
            PlacePickerUtil.placeSearch(this)
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val autoCompleteFragment = fragmentManager.findFragmentById(R.id.test_container) as (PlaceAutocompleteFragment)
        autoCompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(p0: Place?) {
                Log.e("체크", PlacePickerUtil.placeToString(p0!!))
            }
            override fun onError(p0: Status?) {

            }
        })
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {
            val place = PlacePicker.getPlace(this, data)
            tv111.text = PlacePickerUtil.placeToString(place)
        }
    }
}