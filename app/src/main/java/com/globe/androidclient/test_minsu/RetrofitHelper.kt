package com.globe.androidclient.test_minsu

import com.globe.androidclient.R
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Created by baeminsu on 22/07/2018.
 */


class RetrofitHelper() {


    companion object {

        const val KAKAO_REST_API = "50b31d2c92f31dffde365def18c0d969"
        const val KAKAO_BASE_URL = "https://dapi.kakao.com"
        private var retrofit: Retrofit? = null

        fun getKakaoLocalClient(): Retrofit {
            if (retrofit == null) {
                val httpClient = OkHttpClient.Builder()
                httpClient.addInterceptor(object : Interceptor {
                    override fun intercept(chain: Interceptor.Chain?): Response {

                        val original = chain?.request()
                        val request = original!!.newBuilder()
                                .header("Authorization", "KakaoAK $KAKAO_REST_API")
                                .header("Accept", "application/json")
                                .build()
                        return chain.proceed(request)
                    }
                })
                val client = httpClient.build()
                retrofit = Retrofit.Builder()
                        .baseUrl(KAKAO_BASE_URL)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build();
            }
            return retrofit!!
        }
    }


}