package com.globe.androidclient.service

import android.app.Notification
import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.IBinder
import android.util.Log
import com.globe.androidclient.model.local.AppDatabase
import com.globe.androidclient.model.local.GPSTrackDao
import com.globe.androidclient.model.local.schema.GPSTrack
import com.raizlabs.android.dbflow.config.FlowManager
import com.yayandroid.locationmanager.base.LocationBaseService
import com.yayandroid.locationmanager.configuration.DefaultProviderConfiguration
import com.yayandroid.locationmanager.configuration.LocationConfiguration

class GPSTrackingService : LocationBaseService() {

    override fun onBind(intent: Intent): IBinder? = null

    private var isLocationRequested = false

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        if (!isLocationRequested) {
            isLocationRequested = true
            getLocation()
        }
        startForeground(2383, Notification())

        return START_REDELIVER_INTENT
    }

    override fun onDestroy() {
        super.onDestroy()
        stopForeground(true)
    }

    private val ACTION_LOCATION_FAILED = "com.globe.androidclient.LOCATION_FAILED"
    private val EXTRA_FAIL_TYPE = "FAIL_TYPE"

    override fun onLocationFailed(type: Int) {
        val intent = Intent(ACTION_LOCATION_FAILED)
        intent.putExtra(EXTRA_FAIL_TYPE, type)
        sendBroadcast(intent)
        Log.d("asdf", "asdf$type")

        stopSelf()
    }

    override fun onLocationChanged(location: Location?) {
        val gpsTrack = GPSTrack(
                lat = location?.latitude!!,
                lng = location.longitude)
        GPSTrackDao.insertGPSTrack(gpsTrack)
        Log.d("asdf", gpsTrack.toString())
    }

    override fun getLocationConfiguration(): LocationConfiguration
        = LocationConfiguration.Builder()
            .keepTracking(true)
            .useDefaultProviders(DefaultProviderConfiguration.Builder().build())
            .build()
}
