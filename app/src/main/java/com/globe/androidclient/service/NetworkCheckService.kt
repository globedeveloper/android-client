package com.globe.androidclient.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

//        //인터넷 연결 체크
//        val networkCheckIntent = Intent(applicationContext, NetworkCheckService::class.java)
//        networkCheckIntent.apply {
//            putExtra(TAG_INTERVAL, 3L)
//            putExtra(TAG_URL_PING, "https://www.google.com")
//        }
//        networkCheckIntent.putExtra(TAG_ACTIVITY_NAME, this.javaClass.name)
//        startService(networkCheckIntent)


class NetworkCheckService : Service() {

    interface ConnectionServiceCallback {
        fun hasInternetConnection()
        fun hasNoInternetConnection()
    }

    inner class CheckForConnection : TimerTask() {
        override fun run() {
            isNetworkAvailable()
        }
    }

    companion object {
        const val TAG_INTERVAL = "interval"
        const val TAG_URL_PING = "url_ping"
        const val TAG_ACTIVITY_NAME = "activity_name"
    }

    var interval: Long? = null
    lateinit var url_ping: String
    lateinit var activity_name: String
    lateinit var timer: Timer
    private lateinit var connectionServiceCallback: ConnectionServiceCallback

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {


        interval = intent?.getLongExtra(TAG_INTERVAL, 3L)
        url_ping = intent!!.getStringExtra(TAG_URL_PING)
        activity_name = intent.getStringExtra(TAG_ACTIVITY_NAME)

        Log.e("체크", activity_name)
        connectionServiceCallback = Class.forName(activity_name).newInstance() as (ConnectionServiceCallback)

        try {
            connectionServiceCallback = Class.forName(activity_name).newInstance()
                    as ConnectionServiceCallback
        } catch (e: InstantiationException) {
            e.printStackTrace();
        } catch (e: IllegalAccessException) {
            e.printStackTrace();
        } catch (e: ClassNotFoundException) {
            e.printStackTrace();
        }
        timer = Timer()
        timer.scheduleAtFixedRate(CheckForConnection(), 0, interval!! * 1000)
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        timer.cancel()
        super.onDestroy()
    }

    fun isNetworkAvailable(): Boolean {
        val url = URL(url_ping)
        val httpURLConnection: HttpURLConnection = url.openConnection() as HttpURLConnection
        httpURLConnection.apply {
            requestMethod = "GET"
            doInput = false
            doOutput = true
            useCaches = true
            defaultUseCaches = false
            connectTimeout = 5000
        }
        try {
            httpURLConnection.connect()
            connectionServiceCallback.hasInternetConnection()
            onDestroy()
            return true
        } catch (e: Exception) {
            connectionServiceCallback.hasNoInternetConnection()
            onDestroy()
        }
        return false

    }
}


