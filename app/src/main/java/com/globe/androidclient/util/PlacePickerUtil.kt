package com.globe.androidclient.util

import android.app.Activity
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlacePicker
import java.util.*

/**
 * Created by baeminsu on 21/08/2018.
 */

class PlacePickerUtil {

    companion object {
        const val PLACE_PICKER_REQUEST = 1

        fun placeSearch(activity: Activity) {
            PlacePicker.IntentBuilder()

            val intentBuilder = PlacePicker.IntentBuilder()
            try {
                val intent = intentBuilder.build(activity)
                activity.startActivityForResult(intent, PLACE_PICKER_REQUEST)
            } catch (e: GooglePlayServicesRepairableException) {
                e.printStackTrace();
            } catch (e: GooglePlayServicesNotAvailableException) {
                e.printStackTrace();
            }
            //결과값은 각 액티비티 or 프래그먼트에서 forResult로 받아옮
        }
        fun placeToString(place: Place): String {

            val address = place.address.toString()
            val stringTokenizer = StringTokenizer(address, " ")

            val country = stringTokenizer.nextToken()
            val city = stringTokenizer.nextToken()

            return "$country $city"

        }
    }
}