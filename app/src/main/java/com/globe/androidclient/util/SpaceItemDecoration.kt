package com.globe.androidclient.util

import android.graphics.Canvas
import android.graphics.Rect
import android.support.v7.widget.RecyclerView

/**
 * Created by baeminsu on 05/08/2018.
 */
class SpaceItemDecoration(var space : Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect?, itemPosition: Int, parent: RecyclerView?) {
        super.getItemOffsets(outRect, itemPosition, parent)
        outRect!!.set(space,space,space,space)
    }
}