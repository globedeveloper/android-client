package com.globe.androidclient.util


const val KEY_EXAMPLE = "KEY_EXAMPLE"

//START - STOP 관련 상수
const val START_STOP_FLAG = "START_STOP_FLAG"
const val START_DATETIME = "START_DATETIME"
const val STOP_DATETIME = "STOP_DATETIME"
const val START_STOP_ID = "STRAT_STOP_ID"
