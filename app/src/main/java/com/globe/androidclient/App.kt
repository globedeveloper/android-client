package com.globe.androidclient

import android.app.Application
import android.support.multidex.MultiDexApplication
import com.kakao.auth.*
import com.kakao.auth.KakaoSDK
import com.raizlabs.android.dbflow.config.FlowLog
import com.raizlabs.android.dbflow.config.FlowManager

class App: Application() {

    private class KakaoSDKAdapter: KakaoAdapter() {
        override fun getApplicationConfig() = IApplicationConfig { App.instance }

        override fun getSessionConfig() = object : ISessionConfig {
            override fun isSaveFormData() = false

            override fun getAuthTypes() = arrayOf(AuthType.KAKAO_LOGIN_ALL)

            override fun isSecureMode() = false

            override fun getApprovalType() = ApprovalType.INDIVIDUAL

            override fun isUsingWebviewTimer() = true
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        FlowManager.init(this)
        FlowLog.setMinimumLoggingLevel(FlowLog.Level.V)

        KakaoSDK.init(KakaoSDKAdapter())

    }

    companion object {
        lateinit var instance : App private set
    }
}