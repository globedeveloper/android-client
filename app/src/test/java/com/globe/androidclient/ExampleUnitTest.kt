package com.globe.androidclient

import com.globe.androidclient.model.local.schema.GPSTrack
import com.mapbox.geojson.Point
import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun mapListTest() {
        val list = mutableListOf<GPSTrack>()
        list.add(GPSTrack(lat = 36.293231, lng = 37.1823))
        list.add(GPSTrack(lat = 32.293231, lng = 37.1823))
        list.add(GPSTrack(lat = 35.293231, lng = 37.1823))
        list.add(GPSTrack(lat = 37.293231, lng = 37.1823))
        list.add(GPSTrack(lat = 31.293231, lng = 37.1823))
        list.add(GPSTrack(lat = 33.293231, lng = 37.1823))

        val a = list.map {
            Point.fromLngLat(it.lat, it.lng)
        }.toMutableList()
        print(a)
    }

    @Test
    fun calendarTest() {
        val cal = Calendar.getInstance()
        print(cal.time)
    }
}
